# -*- coding: utf-8 -*-
"""
Быстрый алгорим сортировки списка
"""
import random


def my_speed_sort(arr: list):
    """
    Функция быстрой сортировки
    svmitin (c) 2020
    """
    length = len(arr)
    if length < 2:
        return arr
    mid_index = 0
    left, right = [], []
    for i in range(length):
        if arr[i] < arr[mid_index]:
            left.append(arr[i])
        elif i != mid_index:
            right.append(arr[i])
    return my_speed_sort(left) + [arr[mid_index]] + my_speed_sort(right)


def quick_sort_from_book(arr: list) -> list:
    """
    Пример из книги Грокаем алгоритмы
    Краток, но плох тем, что в качестве опортного берется первый элемент списка
    В той-же книге рекомендуется брать случайный или средний элемент
    """
    if len(arr) < 2:
        return arr
    pivot = arr[0]
    left = [i for i in arr[1:] if i < pivot]
    right = [i for i in arr[1:] if i > pivot]
    return my_speed_sort(left) + [pivot] + my_speed_sort(right)


def main():
    """
    Функция-прикол. Очень плохие цифры :)
    """
    print('Search started')
    counter = 0
    while True:
        counter += 1
        unsorted_array = [random.randint(4, 43) for _ in range(6)]
        sorted_array = my_speed_sort(unsorted_array)
        if sorted_array == [4, 8, 15, 16, 23, 42]:
            break
    print('Found in {} iterations'.format(counter))
    print('Source array: {}'.format(unsorted_array))
    print('Sorted array: {}'.format(sorted_array))


if __name__ == '__main__':
    main()
