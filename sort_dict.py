# -*- coding: utf-8 -*-
"""
Сортировка альбомов по году выпуска начиная с самого свежего
"""


def sort_albums_by_year(albums: list, years: list) -> dict:
    """Сортировка альбомов по году выпуска"""
    data = {albums[i]: years[i] for i in range(len(years))}
    result = {album: data[album] for album in sorted(data, key=data.get, reverse=True)}
    return result


if __name__ == '__main__':
    albums_names = ['Sempiternal', 'IMMXRTALISATIXN', 'All hope is gone', 'Mutter', 'Kala']
    albums_years = [2013, 2019, 2008, 2001, 2007]
    print(sort_albums_by_year(albums_names, albums_years))
