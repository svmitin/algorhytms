# -*- coding: utf-8 -*-
"""
Алгоритм поиска наименьшего пути в графе
"""
from collections import deque


class Node:
    """Узел"""

    def __init__(self, name: str, links=None):
        """Создание узла"""
        if links is None:
            links = []
        self.name = name
        self.links = links

    def append_link(self, node_name):
        """Добавить город в соседи"""
        self.links.append(node_name)

    def rewrite_links(self, node_names: list):
        """Добавить город в соседи"""
        self.links = node_names


# Создаем ноды согласно городам GTA SA
# https://gta-real.com/_pu/5/49938733.jpg
LAS_VENTURAS = Node('Las Venturas')
BONE_COUNTY = Node('Bone County')
TIERRA_ROBADA = Node('Tierra Robada')
SAN_FIERRO = Node('San Fierro')
WHETSTONE = Node('Whetstone')
FLINT_COUNTY = Node('Flint County', links=[])
RED_COUNTY = Node('Red County')
LOS_SANTOS = Node('Los Santos')

# Настраиваем связи между городами
LAS_VENTURAS.rewrite_links([BONE_COUNTY, RED_COUNTY])
BONE_COUNTY.rewrite_links([LAS_VENTURAS, RED_COUNTY, TIERRA_ROBADA])
TIERRA_ROBADA.rewrite_links([BONE_COUNTY, SAN_FIERRO])
SAN_FIERRO.rewrite_links([TIERRA_ROBADA, WHETSTONE, FLINT_COUNTY])
WHETSTONE.rewrite_links([SAN_FIERRO, FLINT_COUNTY])
FLINT_COUNTY.rewrite_links([SAN_FIERRO, WHETSTONE, RED_COUNTY])
RED_COUNTY.rewrite_links([SAN_FIERRO, FLINT_COUNTY, LOS_SANTOS, BONE_COUNTY, LAS_VENTURAS])
LOS_SANTOS.rewrite_links([FLINT_COUNTY, RED_COUNTY])


def find_road_with_fired_cities(fire_root: list, start_node: Node, target_node: Node) -> list:
    """
    Поиск пути к городу по цепочке огня
    find_node = [(city: nearby_city_1),
                 (city: nearby_city_2),
                 (city: nearby_city_3)]
    """
    find_node = target_node
    road = []
    while True:
        for fired_city in fire_root:
            if find_node == fired_city[1]:
                road.append(find_node.name)
                find_node = fired_city[0]
                break
            if find_node == start_node:
                road.append(start_node.name)
                return road[::-1]


def search_min_way_to_node(start_node: Node, target_node: Node) -> list:
    """Постепенно "поджигает" города от стартовой точки, пока не подожжет целевой"""
    search_deque = deque()  # Очередь для поджогов
    fired_cities = []       # Те кто уже был подожжен

    # Начинаем поджоги с первого города
    search_deque += start_node.links
    fire_root = [(start_node, i) for i in start_node.links]  # Дерево поджегов (город: сосед)
    while search_deque:
        # Достаем неподожженный город из очереди и поджигаем
        city = search_deque.popleft()
        if city not in fired_cities:
            # Если город является целевым - выводим путь поджогов
            if city.name == target_node.name:
                print('Путь из {} к {} найден:'.format(start_node.name, target_node.name), end=' ')
                return find_road_with_fired_cities(fire_root=fire_root,
                                                   start_node=start_node,
                                                   target_node=target_node)

            # Иначе добавляем город в дерево поджогов
            fire_root += [(city, i) for i in city.links if i != city and i not in fired_cities]
            fired_cities.append(city)
            # Добавляем соседние города в очередь
            for nearby_city in city.links:
                if nearby_city not in fired_cities:
                    search_deque += [nearby_city]
    return []


if __name__ == '__main__':
    print(search_min_way_to_node(WHETSTONE, LAS_VENTURAS))
