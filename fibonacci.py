# -*- coding: utf-8 -*-
"""
Генерирует последовательность чисел Фибоначчи
"""


def generate_fibonacci_list(length: int) -> list:
    """Генерирует последовательность чисел Фибоначчи"""
    result_list = [0]
    for i in range(length):
        if len(result_list) < 2:
            result_list.append(1)
            continue
        new = result_list[len(result_list)-1] + result_list[len(result_list)-2]
        result_list.append(new)
    return result_list


if __name__ == '__main__':
    print(generate_fibonacci_list(length=30))
