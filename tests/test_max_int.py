# -*- coding: utf-8 -*-
"""Тесты для алгоритма поиска наибольшего числа в списке"""
from max_int import find_max


def test_find_max_1():
    assert find_max([9, 3, 5, 2, 6, 1]) == 9


def test_find_max_2():
    assert find_max([-99, 1]) == 1


def test_find_max_3():
    assert find_max([-99, 99, 1]) == 99


def test_find_max_4():
    assert find_max([-99, 1, 99]) == 99
