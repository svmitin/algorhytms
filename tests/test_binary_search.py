# -*- coding: utf-8 -*-
"""Тесты для алгоритма бинарного поиска"""
import binary_search


def test_secret_is_1():
    binary_search.SECRET = 12
    assert binary_search.secret_is(variant=6) == 'Больше'


def test_secret_is_2():
    binary_search.SECRET = 12
    assert binary_search.secret_is(variant=29) == 'Меньше'


def test_secret_is_3():
    binary_search.SECRET = 12
    assert binary_search.secret_is(variant=12) == 'YES'


def test_binary_search_1():
    binary_search.SECRET = 12
    binary_search.MINIMUM = 0
    binary_search.MAXIMUM = 100
    result = binary_search.binary_search(low=binary_search.MINIMUM,
                                         high=binary_search.MAXIMUM)
    assert result == binary_search.SECRET


def test_binary_search_2():
    binary_search.SECRET = 586
    binary_search.MINIMUM = 0
    binary_search.MAXIMUM = 1000000
    result = binary_search.binary_search(low=binary_search.MINIMUM,
                                         high=binary_search.MAXIMUM)
    assert result == binary_search.SECRET


def test_binary_search_3():
    binary_search.SECRET = 53946294
    binary_search.MINIMUM = 0
    binary_search.MAXIMUM = 4000000000
    result = binary_search.binary_search(low=binary_search.MINIMUM,
                                         high=binary_search.MAXIMUM)
    assert result == binary_search.SECRET
