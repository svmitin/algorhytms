# -*- coding: utf-8 -*-
"""Тесты для алгоритма генерации чисел Фибоначчи"""
import fibonacci


def test_generate_fibonacci_list_1():
    assert fibonacci.generate_fibonacci_list(length=7) == [0, 1, 1, 2, 3, 5, 8, 13]


def test_generate_fibonacci_list_2():
    result = fibonacci.generate_fibonacci_list(length=30)
    assert 832040 in result
