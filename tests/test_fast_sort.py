# -*- coding: utf-8 -*-
"""Тесты для быстрой сортировки"""
from fast_sort import my_speed_sort, quick_sort_from_book


def test_my_speed_sort_1():
    assert my_speed_sort(arr=[]) == []


def test_my_speed_sort_2():
    assert my_speed_sort(arr=[1]) == [1]


def test_my_speed_sort_3():
    assert my_speed_sort(arr=[2, 1]) == [1, 2]


def test_my_speed_sort_4():
    assert my_speed_sort(arr=[-2, 1]) == [-2, 1]


def test_my_speed_sort_5():
    assert my_speed_sort(arr=[1, 2, 5, 2, 9, 5, -8]) == [-8, 1, 2, 2, 5, 5, 9]


def test_quick_sort_1():
    assert quick_sort_from_book(arr=[]) == []


def test_quick_sort_2():
    assert quick_sort_from_book(arr=[1]) == [1]


def test_quick_sort_3():
    assert quick_sort_from_book(arr=[2, 1]) == [1, 2]


def test_quick_sort_4():
    assert quick_sort_from_book(arr=[-2, 1]) == [-2, 1]


def test_quick_sort_5():
    assert quick_sort_from_book(arr=[1, 2, 5, 2, 9, 5, -8]) == [-8, 1, 2, 2, 5, 5, 9]
