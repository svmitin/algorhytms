# -*- coding: utf-8 -*-
"""Тесты для алгоритма генерации чисел Фибоначчи"""
import graph_breadth_first_search as bfs


def test_bad_search_min_way_to_node_1():
    """Проверка на существовавшую ранее ошибку"""
    road = bfs.search_min_way_to_node(bfs.BONE_COUNTY, bfs.SAN_FIERRO)
    assert road != ['Bone County', 'Red County', 'Tierra Robada', 'San Fierro']


def test_good_search_min_way_to_node_1():
    """Проверка на корректность пути"""
    road = bfs.search_min_way_to_node(bfs.BONE_COUNTY, bfs.SAN_FIERRO)
    assert road == ['Bone County', 'Red County', 'San Fierro']


def test_good_search_min_way_to_node_2():
    """Проверка на корректность пути"""
    road = bfs.search_min_way_to_node(bfs.LOS_SANTOS, bfs.SAN_FIERRO)
    assert road == ['Los Santos', 'Flint County', 'San Fierro']


def test_good_search_min_way_to_node_3():
    """Проверка на корректность пути"""
    road = bfs.search_min_way_to_node(bfs.WHETSTONE, bfs.LAS_VENTURAS)
    assert road == ['Whetstone', 'Flint County', 'Red County', 'Las Venturas']
