# -*- coding: utf-8 -*-
"""Тесты для алгоритма поиска наибольшего числа в списке"""
from sort_dict import sort_albums_by_year


def test_sort_albums_by_year_1():
    albums_names = ['Sempiternal', 'IMMXRTALISATIXN', 'All hope is gone', 'Mutter', 'Kala']
    albums_years = [2013, 2019, 2008, 2001, 2007]
    result = sort_albums_by_year(albums_names, albums_years)
    assert result == {'IMMXRTALISATIXN': 2019,
                      'Sempiternal': 2013,
                      'All hope is gone': 2008,
                      'Kala': 2007,
                      'Mutter': 2001}


def test_sort_albums_by_year_2():
    albums_names = ['11', '12', '13', '14', '15']
    albums_years = [9, 6, 7, 4, 2]
    result = sort_albums_by_year(albums_names, albums_years)
    assert result == {'15': 2, '14': 4, '12': 6, '13': 7, '11': 9}
