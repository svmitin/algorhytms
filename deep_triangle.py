# -*- coding: utf-8 -*-
"""
Генерирует изображение треугольника
"""
import matplotlib.pyplot as plt

plt.axis([0, 20, 0, 20])


def new_level(cords: tuple, deep: int):
    """Строит новый треугольник"""
    x_cords = [i[0] for i in cords]
    y_cords = [i[1] for i in cords]
    x_cords.append(x_cords[0])
    y_cords.append(y_cords[0])
    plt.plot(x_cords, y_cords)
    # Deep
    if deep > 1:
        # Central
        dot_a = ((x_cords[0] + x_cords[1]) / 2,
                 (y_cords[0] + y_cords[1]) / 2)
        dot_b = ((x_cords[1] + x_cords[2]) / 2,
                 (y_cords[1] + y_cords[2]) / 2)
        dot_c = ((x_cords[2] + x_cords[3]) / 2,
                 (y_cords[2] + y_cords[3]) / 2)
        center = (dot_a, dot_b, dot_c)
        new_level(cords=center, deep=deep - 1)
        # One
        dot_a = (x_cords[0], y_cords[0])
        one = (dot_a, center[0], center[2])
        new_level(cords=one, deep=deep - 1)
        # Two
        dot_b = (x_cords[1], y_cords[1])
        two = (center[0], dot_b, center[1])
        new_level(cords=two, deep=deep - 1)
        # Tree
        dot_c = (x_cords[2], y_cords[2])
        two = (center[1], center[2], dot_c)
        new_level(cords=two, deep=deep - 1)


if __name__ == '__main__':
    MAIN_DOT_A = (1, 1)
    MAIN_DOT_B = (19, 1)
    MAIN_DOT_C = (10, 19)
    MAIN_TRIANGLE = (MAIN_DOT_A,
                     MAIN_DOT_B,
                     MAIN_DOT_C)
    new_level(cords=MAIN_TRIANGLE,
              deep=5)
    plt.show()
