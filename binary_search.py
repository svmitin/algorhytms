# -*- coding: utf-8 -*-
"""Алгоритм бинарного поиска"""
import random


# Количество шагов
steps = 0


def secret_is(variant: int) -> str:
    """Загаданное число это x?"""
    if SECRET > variant:
        return 'Больше'
    if SECRET < variant:
        return 'Меньше'
    return 'YES'


def binary_search(low: int, high: int) -> int:
    """Бинарный поиск числа SECRET из заданного диапазона"""
    global steps
    steps += 1

    middle = (high - low) // 2 + low
    response = secret_is(middle)
    if response == 'YES':
        return middle
    if response == 'Больше':
        return binary_search(low=middle + 1, high=high)
    return binary_search(low=low, high=middle - 1)


if __name__ == '__main__':
    MINIMUM = 0
    MAXIMUM = 1000000
    SECRET = random.randint(MINIMUM, MAXIMUM)
    print('Было загадано число: ', binary_search(MINIMUM, MAXIMUM))
    print('Ответ найден в {} шагов'.format(steps))
