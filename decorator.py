# -*- coding: utf-8 -*-
"""
Простой замеряющий время выполнения функции декоратор с передачей параметров
"""
import time


def benchmark(f):
    def wrapper(*args, **kwargs):
        start = time.time()
        f(*args, **kwargs)
        print('runtime {}() func is'.format(f.__name__), time.time() - start)
    return wrapper


@benchmark
def hello(name: str) -> None:
    """Hello <World>"""
    print('Hello {name}!'.format(name=name))


if __name__ == '__main__':
    hello(name='Core Nobodies')
