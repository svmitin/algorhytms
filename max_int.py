# -*- coding: utf-8 -*-
"""
Простой алгорим поиска наибольшего числа в списке
"""


def find_max(array: list) -> int:
    """Search max int in array"""
    x = array.pop(0)
    if not array:
        return x
    x_max = find_max(array)
    return x if x > x_max else x_max


if __name__ == '__main__':
    print(find_max([1, 4, 2, -2, 0, 7, 9, 3, -5]))
